import ballerina/io;
import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "users" on ep {
    remote function create_user(CreateRequest value) returns CreateResponse|error {
        io:println("received an RPC request from a client...");
        return {userid: value.username};
    }
}
